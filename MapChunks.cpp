#include "MapChunks.hpp"
#include "Texture.hpp"
#include "MathUtilities.hpp"
#include "Utilities.hpp"
#include "World.hpp"
#include "IntVec3.hpp"
#include "Noise.hpp"
#include "Vertex.hpp"
#include "Renderer.hpp"
#include <vector>



extern PFNGLGENBUFFERSPROC		glGenBuffers;
extern PFNGLBINDBUFFERPROC		glBindBuffer;
extern PFNGLBUFFERDATAPROC		glBufferData;
extern PFNGLGENERATEMIPMAPPROC	glGenerateMipmap;

extern Texture* g_textureAtlas;
extern World* g_theWorld;
extern Renderer* g_theRenderer;


MapChunk::MapChunk(IntVec2 chunkCoordinates)
	: m_amountOfVertexes(0)
{
	InitializeID();
	m_isChunkVBODirty = true;
	m_chunkCoords = chunkCoordinates;

	if( !LoadChunkFromFile(chunkCoordinates) )
		GenerateChunkFromNoise();

}


bool MapChunk::LoadChunkFromFile(const ChunkCoords& chunkCoordinates)
{
	std::string fileName = Stringf( "Chunks/Chunk%i,%i.chunk", chunkCoordinates.x, chunkCoordinates.y);
	FILE* file = NULL;
	errno_t theFile = fopen_s(&file, fileName.c_str(), "rb");

	//If the File doesn't exist then return False
	if( theFile != 0 )
		return false;

	unsigned char currentType;
	unsigned short howManyOfCurrentType = 0;
	int currentBufferReadIndex = 0;

	fseek( file, 0 ,SEEK_END);
	int bufferSizeInBytes = ftell(file);
	rewind(file);

	//Putting the File in the Buffer so we can look at it
	unsigned char* buffer = new unsigned char[bufferSizeInBytes];
	fread(buffer, sizeof( unsigned char), bufferSizeInBytes, file);
	fclose(file);

	currentType = buffer[currentBufferReadIndex];
	++currentBufferReadIndex;
	unsigned short* destination = &howManyOfCurrentType;
	unsigned char* source = &buffer[currentBufferReadIndex];
	memcpy(destination, source, sizeof(unsigned short));
	currentBufferReadIndex += sizeof(unsigned short);

	for( int blockIndex = 0; blockIndex < BLOCKS_PER_CHUNK; blockIndex++)
	{
		//Get the block
		Block& generatedBlock = m_blocks[blockIndex];
		generatedBlock.m_blockType = currentType;
		howManyOfCurrentType -= 1;

		if(howManyOfCurrentType == 0)
		{
			currentType = buffer[currentBufferReadIndex];
			++currentBufferReadIndex;
			destination = &howManyOfCurrentType;
			source =  &buffer[currentBufferReadIndex];
			memcpy(destination, source, sizeof(unsigned short));
			currentBufferReadIndex += sizeof(unsigned short);
		}
	}

	delete [] buffer;

	return true;
}


void MapChunk::GenerateChunkFromNoise()
{
	int groundHeight = 64;

	for(int columnIndex = 0; columnIndex < BLOCK_LEVEL_TOTALS; columnIndex++)
	{
		float noise = ComputePerlinNoiseValueAtPosition2D( GetBlockWorldCoordsFromIndex(columnIndex), 10.f, 5, 5.f, 0.4f);
		float blockHeight = groundHeight + noise;
		columnHeights[columnIndex] = blockHeight;
	}


	for( int blockIndex = 0; blockIndex < BLOCKS_PER_CHUNK; blockIndex++)
	{
		Block& block = m_blocks[blockIndex];
		IntVec3 localCoordinates = GetLocalCoordsFromBlockIndex(blockIndex);
		int columnIndex = localCoordinates.x + (localCoordinates.y * BLOCKS_WIDE);
		int individualColumnHeight = static_cast<int>(columnHeights[columnIndex]);
		int dirtLevel = individualColumnHeight - 5;



		if( localCoordinates.z > individualColumnHeight )
			block.m_blockType = AIR;
		else if( localCoordinates.z < dirtLevel )
			block.m_blockType = STONE;
		else
			block.m_blockType = DIRT;

		//Set the Light Levels for Each Block, DO THIS IN THE LOAD CHUNK FROM FILE AS WELL
		block.m_blockLightLevel = 0;
		if( block.m_blockType == GLOWSTONE )
			block.m_blockLightLevel = 12;
	}
}


//Creating the VBO ID for each individual Chunk
void MapChunk::InitializeID()
{
	m_vboID = 0;
	glGenBuffers(1, &m_vboID);
}


//Getting the World Coordinates of a Block
vec2 MapChunk::GetBlockWorldCoordsFromIndex(int blockIndex)
{
	IntVec3 localCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	vec2 temp;
	temp.x = static_cast<float>(localCoords.x + (BLOCKS_WIDE * m_chunkCoords.x));
	temp.y = static_cast<float>(localCoords.y + (BLOCKS_WIDE * m_chunkCoords.y));
	return temp;
}


vec3 MapChunk::GetBlockWorldCoordsFromIndexToVec3(int blockIndex)
{
	IntVec3 localCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	vec3 temp;
	temp.x = static_cast<float>(localCoords.x + (BLOCKS_WIDE * m_chunkCoords.x));
	temp.y = static_cast<float>(localCoords.y + (BLOCKS_WIDE * m_chunkCoords.y));
	temp.z = static_cast<float>(localCoords.z);
	return temp;
}


IntVec2 MapChunk::GetChunkWorldCoordsFromLocalCoords(ChunkCoords theChunk)
{
	IntVec2 temp;
	temp.x = BLOCKS_WIDE * theChunk.x;
	temp.y = BLOCKS_WIDE * theChunk.y;
	return temp;
}


//Shifting Left doubles the number, shifting right halves the number. 6 << 1 = 12, 6 >> 1 = 3
const IntVec3 MapChunk::GetLocalCoordsFromBlockIndex( const int localIndex)
{
	const unsigned int BOTTOM_4_BITS_MASK = 15;
	IntVec3 localCoords;
	localCoords.x = (localIndex & BOTTOM_4_BITS_MASK);
	localCoords.y = (localIndex >> 4) & BOTTOM_4_BITS_MASK;
	localCoords.z = (localIndex >> 8);
	return localCoords;
}


IntVec3 MapChunk::GetLocalCoordinatesForBlockWorldCoords(const IntVec3& blockWorldCoords)
{
	IntVec3 temp;
	temp.x = blockWorldCoords.x & 15;
	temp.y = blockWorldCoords.y & 15;
	temp.z = blockWorldCoords.z;
	return temp;
}


IntVec3 MapChunk::GetLocalCoordsFromWorldCoords(const vec3& worldCoords)
{
	vec3 cameraWorldCoords = worldCoords;

	while( cameraWorldCoords.x < 0)
		cameraWorldCoords.x += BLOCKS_WIDE;

	while( cameraWorldCoords.y < 0)
		cameraWorldCoords.y += BLOCKS_WIDE;

	if( cameraWorldCoords.z < 0)
		cameraWorldCoords.z = 0;
	else if( cameraWorldCoords.z > static_cast<float>(BLOCKS_TALL) )
	{
		cameraWorldCoords.z = BLOCKS_TALL - 1;
	}


	IntVec3 cameraPosition = IntVec3( static_cast<int>(cameraWorldCoords.x) % BLOCKS_WIDE, static_cast<int>(cameraWorldCoords.y) % BLOCKS_WIDE , static_cast<int>(cameraWorldCoords.z) );

	return cameraPosition;
}


//Getting the Block Index from the Blocks Coordinates Local to the Chunk
int MapChunk::GetBlockIndexFromBlockLocalCoords(const IntVec3& blockLocalCoords)
{
	int blockIndex = (BLOCK_LEVEL_TOTALS * blockLocalCoords.z) + (BLOCKS_WIDE * blockLocalCoords.y) + blockLocalCoords.x;

	return blockIndex;
}


int MapChunk::GetBlockIndexFromBlockLocalCoordsWithVec3(const vec3& blockLocalCoords)
{
	int blockIndex = (BLOCK_LEVEL_TOTALS * static_cast<int>(blockLocalCoords.z)) + (BLOCKS_WIDE * static_cast<int>(blockLocalCoords.y)) + static_cast<int>(blockLocalCoords.x);

	return blockIndex;
}


ChunkCoords MapChunk::GetChunkCoordsFromWorldCoords( const vec2& worldPosition)
{
	int xCoord = static_cast<int>(floor(worldPosition.x));
	int yCoord = static_cast<int>(floor(worldPosition.y));

	xCoord = xCoord >> 4;
	yCoord = yCoord >> 4;

	return ChunkCoords(xCoord, yCoord);
}


void MapChunk::Render()
{
	if( m_isChunkVBODirty == true)
		ApplyTextureVertexesToBlocks();

	g_theRenderer->CallGlEnableTexture2D();
	glBindTexture(GL_TEXTURE_2D, g_textureAtlas->m_openglTextureID);
	glBindBuffer( GL_ARRAY_BUFFER, m_vboID );

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	//For Vertex Arrays
// 	glVertexPointer( 3, GL_FLOAT, sizeof( Vertex ), &m_vertexes[0].position );
// 	glTexCoordPointer( 2, GL_FLOAT, sizeof( Vertex ), &m_vertexes[0].texCoords );
// 	glColorPointer(	4, GL_FLOAT, sizeof( Vertex ), &m_vertexes[0].color );

	//For VBOs
	glVertexPointer( 3, GL_FLOAT, sizeof( Vertex ), (const GLvoid*) offsetof( Vertex, position) );
	glTexCoordPointer( 2, GL_FLOAT, sizeof( Vertex ), (const GLvoid*) offsetof( Vertex, texCoords) );
	glColorPointer(	4, GL_FLOAT, sizeof( Vertex ), (const GLvoid*) offsetof( Vertex, color) );

	glDrawArrays( GL_QUADS, 0, m_amountOfVertexes );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}


void MapChunk::ApplyTextureVertexesToBlocks()
{
	int blockIndex = 0;
	std::vector< Vertex >* ptrToNewVertexes = new std::vector< Vertex >;
	std::vector< Vertex >& vertexes = *ptrToNewVertexes;

	vertexes.clear();

	for( int blockZCoord = 0; blockZCoord < BLOCKS_TALL; blockZCoord++)
	{
		for( int blockYCoord = 0; blockYCoord < BLOCKS_WIDE; blockYCoord++)
		{
			for(int blockXCoord = 0; blockXCoord < BLOCKS_WIDE; blockXCoord++)
			{
				m_blocks[blockIndex].Render(blockXCoord, blockYCoord, blockZCoord, vertexes, g_theWorld->m_chunks[m_chunkCoords]);
				blockIndex++;
			}
		}
	}

	m_amountOfVertexes = static_cast<int>(vertexes.size());

	glBindBuffer( GL_ARRAY_BUFFER, m_vboID );
	glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex ) * m_amountOfVertexes, vertexes.data(), GL_STATIC_DRAW );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	delete ptrToNewVertexes;
	m_isChunkVBODirty = false;
}


//Getting Block Neighbors
Block* MapChunk::GetNeighborBlockAbove(int blockIndex)
{
	IntVec3 localBlockCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	//If the block is on the top level then there is nothing above it.
	if( localBlockCoords.z >= (BLOCKS_TALL - 1 ) )
		return nullptr;
	else
	{
		Block* blockFromAbove = &m_blocks[blockIndex + BLOCK_LEVEL_TOTALS];
		return blockFromAbove;
	}
}


Block* MapChunk::GetNeighborBlockToTheRight(int blockIndex)
{
	IntVec3 localBlockCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	//If it is on the edge of the chunk
	if( localBlockCoords.x >= (BLOCKS_WIDE - 1) )
	{
		//Check to see if there is a neighboring chunk to the Right
		vec2 BlockWorldPosition = GetBlockWorldCoordsFromIndex(blockIndex);
		ChunkCoords ChunkCoordinates = GetChunkCoordsFromWorldCoords(BlockWorldPosition);
		ChunkCoords ChunkToTheRightCoords = ChunkCoords(ChunkCoordinates.x + 1, ChunkCoordinates.y);

		//If the Chunk to the right exists then we need to get the block at the beginning of the layer that the first block is on.
		if( g_theWorld->m_chunks.find(ChunkToTheRightCoords) != g_theWorld->m_chunks.end() )
		{
			MapChunk* chunktoRight = g_theWorld->m_chunks[ChunkToTheRightCoords];
			Block* blockToRight = &chunktoRight->m_blocks[(blockIndex - (BLOCKS_WIDE - 1))];
			return blockToRight;
		}
		else	//if it doesn't find it in the map then it doesn't real.
		{
			return nullptr;
		}

	}
	else	//If the block isn't on the edge
	{
		Block* blockToRight = &m_blocks[(blockIndex + 1)];
		return blockToRight;
	}
}


Block* MapChunk::GetBlockNeighborBelow(int blockIndex)
{
	IntVec3 localBlockCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	//If it is on the first block layer
	if( localBlockCoords.z == 0 )
		return nullptr;
	else
	{
		Block* blockFromBelow = &m_blocks[(blockIndex - BLOCK_LEVEL_TOTALS)];
		return blockFromBelow;
	}
}


Block* MapChunk::GetBlockNeighborToLeft(int blockIndex)
{
	IntVec3 localBlockCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	//If it is on the edge of the chunk on the left side
	if( localBlockCoords.x <= 0 )
	{
		//Check to see if there is a neighboring chunk to the Left
		vec2 BlockWorldPosition = GetBlockWorldCoordsFromIndex(blockIndex);
		ChunkCoords ChunkCoordinates = GetChunkCoordsFromWorldCoords(BlockWorldPosition);
		ChunkCoords ChunkToTheLeftCoords = ChunkCoords(ChunkCoordinates.x - 1, ChunkCoordinates.y);

		//If the Chunk to the right exists then we need to get the block at the beginning of the layer that the first block is on.
		if( g_theWorld->m_chunks.find(ChunkToTheLeftCoords) != g_theWorld->m_chunks.end() )
		{
			MapChunk* chunktoLeft = g_theWorld->m_chunks[ChunkToTheLeftCoords];
			Block* blockToLeft = &chunktoLeft->m_blocks[(blockIndex + (BLOCKS_WIDE - 1))];
			return blockToLeft;
		}
		else	//if it doesn't find it in the map then it doesn't real.
		{
			return nullptr;
		}

	}

	else	//If the block isn't on the edge
	{
		Block* blockToLeft = &m_blocks[(blockIndex - 1)];
		return blockToLeft;
	}
}


Block* MapChunk::GetBlockNeighborInFront(int blockIndex)
{
	IntVec3 localBlockCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	//If the block is along the front edge of the chunk, then we need to check for the next chunk.
	if( localBlockCoords.y >= (BLOCKS_WIDE - 1) )
	{
		//Check to see if there is a neighboring chunk in front
		vec2 BlockWorldPosition = GetBlockWorldCoordsFromIndex(blockIndex);
		ChunkCoords ChunkCoordinates = GetChunkCoordsFromWorldCoords(BlockWorldPosition);
		ChunkCoords ChunkInFrontCoords = ChunkCoords(ChunkCoordinates.x, ChunkCoordinates.y + 1);

		//If the Chunk to the right exists then we need to get the block at the beginning of the layer that the first block is on.
		if( g_theWorld->m_chunks.find(ChunkInFrontCoords) != g_theWorld->m_chunks.end() )
		{
			MapChunk* chunkInFront = g_theWorld->m_chunks[ChunkInFrontCoords];
			Block* blockInFront = &chunkInFront->m_blocks[blockIndex - (BLOCKS_WIDE * (BLOCKS_WIDE - 1))];
			return blockInFront;
		}
		else	//if it doesn't find it in the map then it doesn't real.
		{
			return nullptr;
		}
	}
	else //If the block is not on the edge
	{
		Block* blockInFront = &m_blocks[(blockIndex + BLOCKS_WIDE)];
		return blockInFront;
	}

}


Block* MapChunk::GetBlockNeighborBehind(int blockIndex)
{
	IntVec3 localBlockCoords = GetLocalCoordsFromBlockIndex(blockIndex);

	//If the block is along the back edge of the chunk, then we need to check for the next chunk.
	if( localBlockCoords.y <= 0 )
	{
		//Check to see if there is a neighboring chunk behind us
		vec2 BlockWorldPosition = GetBlockWorldCoordsFromIndex(blockIndex);
		ChunkCoords ChunkCoordinates = GetChunkCoordsFromWorldCoords(BlockWorldPosition);
		ChunkCoords ChunkBehindCoords = ChunkCoords(ChunkCoordinates.x, ChunkCoordinates.y - 1);

		//If the Chunk to the south of us exists then we need to get the block at the beginning of the layer that the first block is on.
		if( g_theWorld->m_chunks.find(ChunkBehindCoords) != g_theWorld->m_chunks.end() )
		{
			MapChunk* chunkBehind = g_theWorld->m_chunks[ChunkBehindCoords];
			Block* blockBehind = &chunkBehind->m_blocks[blockIndex + (BLOCKS_WIDE * (BLOCKS_WIDE - 1))];
			return blockBehind;
		}
		else	//if it doesn't find it in the map then it doesn't real.
		{
			return nullptr;
		}
	}
	else //If the block is not on the edge
	{
		Block* blockBehind = &m_blocks[(blockIndex - BLOCKS_WIDE)];
		return blockBehind;
	}
}
