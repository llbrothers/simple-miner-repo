//These are the functions from the World.cpp that pretained to the functionality of the MapChunks



void World::GenerateOrLoadChunk(const ChunkCoords& chunkCoordinates)
{
	MapChunk* newChunk = new MapChunk(chunkCoordinates);
	m_chunks[chunkCoordinates] = newChunk;
	m_chunksToGenerateOrLoad.erase(chunkCoordinates);
	DetermineSkyBlocks(chunkCoordinates);
	MarkingNonSkyAirBlocksDirty(chunkCoordinates);
	MarkAirBlocksOnChunkBoundariesDirty(chunkCoordinates);
	DetermineGrassBlocks(chunkCoordinates);


	//Setting Neighbor VBO's to Dirty
	if( m_chunks.find(ChunkCoords(chunkCoordinates.x+1, chunkCoordinates.y)) != m_chunks.end() )
	{
		MapChunk* EastChunk = m_chunks[(ChunkCoords(chunkCoordinates.x+1, chunkCoordinates.y))];
		EastChunk->m_isChunkVBODirty = true;
	}

	if( m_chunks.find(ChunkCoords(chunkCoordinates.x-1, chunkCoordinates.y)) != m_chunks.end() )
	{
		MapChunk* WestChunk = m_chunks[(ChunkCoords(chunkCoordinates.x-1, chunkCoordinates.y))];
		WestChunk->m_isChunkVBODirty = true;
	}

	if( m_chunks.find(ChunkCoords(chunkCoordinates.x, chunkCoordinates.y+1)) != m_chunks.end() )
	{
		MapChunk* NorthChunk = m_chunks[(ChunkCoords(chunkCoordinates.x, chunkCoordinates.y+1))];
		NorthChunk->m_isChunkVBODirty = true;
	}

	if( m_chunks.find(ChunkCoords(chunkCoordinates.x, chunkCoordinates.y-1)) != m_chunks.end() )
	{
		MapChunk* SouthChunk = m_chunks[(ChunkCoords(chunkCoordinates.x, chunkCoordinates.y-1))];
		SouthChunk->m_isChunkVBODirty = true;
	}

}


void World::GenerateOrLoadNextChunkToDo()
{
	const float REALLY_FAR = FLT_MAX;
	float closestChunkDistanceSquared = REALLY_FAR;

	ChunkCoords closestChunkCoords = IntVec2(0,0);
	//m_chunksToGenerateOrLoad.insert(closestChunkCoords); //For Debugging
	std::set<ChunkCoords>::iterator iter;

	for( iter = m_chunksToGenerateOrLoad.begin(); iter != m_chunksToGenerateOrLoad.end(); ++ iter)
	{
		const ChunkCoords& chunkCoordinates = *iter;
		float distSquaredToChunk = CalcDistSquaredToChunkCoords(chunkCoordinates);
		if( distSquaredToChunk < closestChunkDistanceSquared )
		{
			if( m_chunks.find(chunkCoordinates) == m_chunks.end() )
			{
				closestChunkDistanceSquared = distSquaredToChunk;
				closestChunkCoords = chunkCoordinates;
			}
		}
	}

	if( closestChunkDistanceSquared < REALLY_FAR )
	{
		GenerateOrLoadChunk(closestChunkCoords);
	}
}


void World::CreateChunksWithinCameraRadius()
{
	//Get the Camera's World Coordinates
	IntVec2 cameraChunkCoords = GetChunkCoordsFromCameraPosition();
	for( int chunkYCoord = 0; chunkYCoord < g_camera.m_cameraVisionRadiusInChunks; chunkYCoord++)
	{
		for( int chunkXCoord = 0; chunkXCoord < g_camera.m_cameraVisionRadiusInChunks; chunkXCoord++)
		{
			//Create the coordinates for the area around the Camera (Front and Back)
			ChunkCoords InFrontOfCamera = IntVec2(chunkXCoord + cameraChunkCoords.x , chunkYCoord + cameraChunkCoords.y );
			ChunkCoords BehindCamera = IntVec2(-chunkXCoord + cameraChunkCoords.x , -chunkYCoord + cameraChunkCoords.y );
			ChunkCoords ToLeftOfCamera = IntVec2(-chunkXCoord + cameraChunkCoords.x , chunkYCoord + cameraChunkCoords.y );
			ChunkCoords ToRightOfCamera = IntVec2(chunkXCoord + cameraChunkCoords.x , -chunkYCoord + cameraChunkCoords.y );

			//Put them in the To Do List to be generated or Loaded
			m_chunksToGenerateOrLoad.insert(InFrontOfCamera);
			m_chunksToGenerateOrLoad.insert(BehindCamera);
			m_chunksToGenerateOrLoad.insert(ToLeftOfCamera);
			m_chunksToGenerateOrLoad.insert(ToRightOfCamera);
		}
	}
}


void World::DeleteChunkFromWorld()
{
	float cameraOuterRadiusInChunks = g_camera.m_cameraVisionRadiusInChunks + 2.f;
	float cameraOuterRadiusInBlocks = cameraOuterRadiusInChunks * BLOCKS_WIDE;

	const float MINIMUM_DELETE_DISTANCE_SQUARED = cameraOuterRadiusInBlocks * cameraOuterRadiusInBlocks;
	float closestChunkDistanceSquared = MINIMUM_DELETE_DISTANCE_SQUARED;

	ChunkCoords furthestChunkCoords = IntVec2(0,0);
	std::map< ChunkCoords, MapChunk* >::iterator iter;

	for( iter = m_chunks.begin(); iter != m_chunks.end(); ++ iter)
	{
		const ChunkCoords& chunkCoordinates = iter->first;
		float distSquaredToChunk = CalcDistSquaredToChunkCoords(chunkCoordinates);

		if( distSquaredToChunk > closestChunkDistanceSquared )
		{
			if( m_chunks.find(chunkCoordinates) != m_chunks.end() )
			{
				closestChunkDistanceSquared = distSquaredToChunk;
				furthestChunkCoords = chunkCoordinates;
			}
		}
	}

	if( closestChunkDistanceSquared > MINIMUM_DELETE_DISTANCE_SQUARED )
	{
		SaveChunkToFile(furthestChunkCoords);

		std::map< ChunkCoords, MapChunk* >::iterator found = m_chunks.find(furthestChunkCoords);
		glDeleteBuffers(1, &found->second->m_vboID);
		delete found->second;
		m_chunks.erase(found);

		m_chunksToSaveAndDelete.erase(furthestChunkCoords);
	}
}


void World::SaveChunkToFile(const ChunkCoords& chunkCoordinates)
{
	//Buffer for the file
	int out_numBytesInBuffer = 0;
	unsigned char* bufferForChunkFile = CreateNewRLEBufferForChunk(chunkCoordinates, out_numBytesInBuffer);


	std::string fileName = Stringf( "Chunks/Chunk%i,%i.chunk", chunkCoordinates.x, chunkCoordinates.y);
	FILE* file = NULL;
	//c_str gives the const char* of the String
	errno_t theFile = fopen_s(&file, fileName.c_str(), "wb");

	if( theFile == 0 )
	{		
		fwrite(bufferForChunkFile, 1, out_numBytesInBuffer, file );
		fclose(file);
	}

	DeleteChunkFileBufferFromRLE(bufferForChunkFile);
}


void World::DeleteChunkFileBufferFromRLE(unsigned char* buffer)
{
	delete [] buffer;
}


//Creating the RLE and Calculating Chunk Coordinates
unsigned char* World::CreateNewRLEBufferForChunk(const ChunkCoords& chunkCoordinates, int& out_numBytesInBuffer)
{
	MapChunk* theChunk = m_chunks[chunkCoordinates];

	unsigned char currentType = theChunk->m_blocks[0].m_blockType;
	unsigned short howManyOfCurrentType = 1;
	const int bufferSizeInBytes = BLOCKS_PER_CHUNK * 3;
	int currentBufferWriteIndex = 0;

	unsigned char* buffer = new unsigned char[bufferSizeInBytes];

	for( int blockIndex = 1; blockIndex < BLOCKS_PER_CHUNK; blockIndex++)
	{
		Block& currentBlock = theChunk->m_blocks[blockIndex];

		if( currentBlock.m_blockType != currentType )
		{
			buffer[currentBufferWriteIndex] = currentType;
			++currentBufferWriteIndex;
			unsigned char* destination = buffer + currentBufferWriteIndex;
			unsigned short* source = &howManyOfCurrentType;
			memcpy( destination, source, sizeof(unsigned short) );
			currentBufferWriteIndex += sizeof( unsigned short );

			currentType = currentBlock.m_blockType;
			howManyOfCurrentType = 1;
		}
		else
			++howManyOfCurrentType;
	}

	buffer[currentBufferWriteIndex] = currentType;
	++currentBufferWriteIndex;
	unsigned char* destination = buffer + currentBufferWriteIndex;
	unsigned short* source = &howManyOfCurrentType;
	memcpy( destination, source, sizeof(unsigned short) );
	currentBufferWriteIndex += sizeof( unsigned short );


	out_numBytesInBuffer = currentBufferWriteIndex;
	return buffer;
}
