#ifndef INCLUDE_BLOCK
#define INCLUDE_BLOCK

#include "IntVector2.hpp"
#include "Vector2.hpp"
#include "Vertex.hpp"
#include <vector>

//Just to make Render happy, let's it know that MapChunk is a real thing
class MapChunk;

enum BLOCK_TYPES
{
	AIR,
	DIRT,
	STONE,
	GLOWSTONE,
	GRASS,
	NUM_BLOCK_TYPES
};


class Block
{
public:
	Block();
	~Block() {};

	void Render(int xCoord, int yCoord, int zCoord, std::vector< Vertex >& vertexes, MapChunk* currentChunk);
	void Update();

	//Getting Texture Coordinates
	vec2 GetTexCoordMinsForTextureAtlasPosition( IntVec2 TexPosition );
	vec2 GetSideMinCoordsForTextureBlock(unsigned char blockType);
	vec2 GetTopMinCoordsForTextureBlock(unsigned char blockType);
	vec2 GetBottomMinCoordsForTextureBlock(unsigned char blockType);

	//Acting On Blocks
	void BreakABlock();
	void PlaceABlock();



	unsigned char m_blockType;
	unsigned char m_blockLightLevel;	//For determining the Light Value
	unsigned char m_isSky;
};
#endif