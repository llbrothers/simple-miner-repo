#include "Block.hpp"
#include "Vector2.hpp"
#include "IntVector2.hpp"
#include "vec3.hpp"
#include "vec4.hpp"
#include "MapChunks.hpp"
#include "Vertex.hpp"



const float ADJUST_BRIGHTNESS = .50f;


Block::Block()
{
	m_isSky = 0;
	m_blockLightLevel = 0;
}


void Block::Render(int xCoord, int yCoord, int zCoord, std::vector< Vertex >& vertexes, MapChunk* currentChunk)
{
	Vertex vertexNew;

	vec3 blockLocalPositionInChunk = vec3(static_cast<float>(xCoord), static_cast<float>(yCoord), static_cast<float>(zCoord));
	int currentBlockIndex = currentChunk->GetBlockIndexFromBlockLocalCoordsWithVec3(blockLocalPositionInChunk);

	//Block Neighbors
	Block* blockSouthOfCurrentBlock = currentChunk->GetBlockNeighborBehind(currentBlockIndex);
	Block* blockAboveCurrentBlock = currentChunk->GetNeighborBlockAbove(currentBlockIndex);
	Block* blockBelowCurrentBlock = currentChunk->GetBlockNeighborBelow(currentBlockIndex);
	Block* blockLeftOfCurrentBlock = currentChunk->GetBlockNeighborToLeft(currentBlockIndex);
	Block* blockRightOfCurrentBlock = currentChunk->GetNeighborBlockToTheRight(currentBlockIndex);
	Block* blockNorthOfCurrentBlock = currentChunk->GetBlockNeighborInFront(currentBlockIndex);

	if( m_blockType == AIR )
		return;

	float individualTileSize = (1.f/32.f);

	//Sides
	vec2 sideBlockMins = GetSideMinCoordsForTextureBlock(m_blockType);
	vec2 sideBlockMax = vec2(sideBlockMins.x + individualTileSize, sideBlockMins.y + individualTileSize);

	//Top
	vec2 topBlockMins = GetTopMinCoordsForTextureBlock(m_blockType);
	vec2 topBlockMax = vec2( topBlockMins.x + individualTileSize, topBlockMins.y + individualTileSize);

	//Bottom
	vec2 bottomDirtMins = GetBottomMinCoordsForTextureBlock(m_blockType);
	vec2 bottomDirtMax = vec2( bottomDirtMins.x +individualTileSize, bottomDirtMins.y + individualTileSize);

	//Creating the different Vertices

		//Setting the Color to White, or Green for Grass

	if( m_blockType == GRASS )
		vertexNew.color = vec4(0.f, 1.f, 0.f, 1.f);
	else
		vertexNew.color = vec4(1.f,1.f,1.f,1.f);


		//SOUTH / BACK Face
		if( blockSouthOfCurrentBlock != nullptr && (blockSouthOfCurrentBlock->m_blockType == AIR || blockSouthOfCurrentBlock->m_blockType == GRASS)  )
		{
			if( blockSouthOfCurrentBlock->m_isSky == 1 )
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, 1.f, 0.f, 1.f);
				else
					vertexNew.color = vec4(1.f,1.f,1.f,1.f);
			}
			else
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, (1.0f / blockSouthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 0.f, 1.0f);
				else
					vertexNew.color = vec4((1.0f / blockSouthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockSouthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockSouthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 1.0f);
			}

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,0.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,0.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,0.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,0.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);
		}


		// 		//-----------------------------------------------------------------//
		// 		//NORTH / FRONT face
		if( blockNorthOfCurrentBlock != nullptr && (blockNorthOfCurrentBlock->m_blockType == AIR || blockNorthOfCurrentBlock->m_blockType == GRASS) )
		{
			if( blockNorthOfCurrentBlock->m_isSky == 1 )
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, 1.f, 0.f, 1.f);
				else
					vertexNew.color = vec4(1.f,1.f,1.f,1.f);
			}
			else
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, (1.0f / blockNorthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 0.f, 1.0f);
				else
					vertexNew.color = vec4((1.0f / blockNorthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockNorthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockNorthOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 1.0f);
			}

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,1.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,1.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,1.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,1.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);
		}


		// 		//-----------------------------------------------------------------//
		// 		//LEFT / WEST face
		if( blockLeftOfCurrentBlock != nullptr && (blockLeftOfCurrentBlock->m_blockType == AIR || blockLeftOfCurrentBlock->m_blockType == GRASS)  )
		{
			if( blockLeftOfCurrentBlock->m_isSky == 1 )
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, 1.f, 0.f, 1.f);
				else
					vertexNew.color = vec4(1.f,1.f,1.f,1.f);
			}
			else
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, (1.0f / blockLeftOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 0.f, 1.0f);
				else
					vertexNew.color = vec4((1.0f / blockLeftOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockLeftOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockLeftOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 1.0f);
			}


			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,1.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,0.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,0.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,1.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);
		}


		// 		//-----------------------------------------------------------------//
		// 		//EAST / RIGHT Face
		if( blockRightOfCurrentBlock != nullptr && (blockRightOfCurrentBlock->m_blockType == AIR || blockRightOfCurrentBlock->m_blockType == GRASS)  )
		{
			if( blockRightOfCurrentBlock->m_isSky == 1 )
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, 1.f, 0.f, 1.f);
				else
					vertexNew.color = vec4(1.f,1.f,1.f,1.f);
			}
			else
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, (1.0f / blockRightOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 0.f, 1.0f);
				else
					vertexNew.color = vec4((1.0f / blockRightOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockRightOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockRightOfCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 1.0f);
			}


			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,0.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,1.f,0.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,1.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMax.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,0.f,1.f);
			vertexNew.texCoords = vec2(sideBlockMins.x, sideBlockMins.y);
			vertexes.push_back(vertexNew);

		}


		// 		//-----------------------------------------------------------------//
		// 		//Bottom Face
		if( blockBelowCurrentBlock != nullptr && (blockBelowCurrentBlock->m_blockType == AIR  || blockBelowCurrentBlock->m_blockType == GRASS) )
		{
			if( blockBelowCurrentBlock->m_isSky == 1 )
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, 1.f, 0.f, 1.f);
				else
					vertexNew.color = vec4(1.f,1.f,1.f,1.f);
			}
			else
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, (1.0f / blockBelowCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 0.f, 1.0f);
				else
					vertexNew.color = vec4((1.0f / blockBelowCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockBelowCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockBelowCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 1.0f);
			}

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,1.f,0.f);
			vertexNew.texCoords = vec2(bottomDirtMins.x, bottomDirtMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(1.f,0.f,0.f);
			vertexNew.texCoords = vec2(bottomDirtMax.x, bottomDirtMax.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,0.f,0.f);
			vertexNew.texCoords = vec2(bottomDirtMax.x, bottomDirtMins.y);
			vertexes.push_back(vertexNew);

			vertexNew.position = blockLocalPositionInChunk + vec3(0.f,1.f,0.f);
			vertexNew.texCoords = vec2(bottomDirtMins.x, bottomDirtMins.y);
			vertexes.push_back(vertexNew);
		}


		//-----------------------------------------------------------------//
		//Top Face
		if( blockAboveCurrentBlock != nullptr && (blockAboveCurrentBlock->m_blockType == AIR || blockAboveCurrentBlock->m_blockType == GRASS) )
		{
			if( blockAboveCurrentBlock->m_isSky == 1 )
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, 1.f, 0.f, 1.f);
				else
					vertexNew.color = vec4(1.f,1.f,1.f,1.f);
			}
			else
			{
				if( m_blockType == GRASS )
					vertexNew.color = vec4(0.f, (1.0f / blockAboveCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 0.f, 1.0f);
				else
					vertexNew.color = vec4((1.0f / blockAboveCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockAboveCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, (1.0f / blockAboveCurrentBlock->m_blockLightLevel) + ADJUST_BRIGHTNESS, 1.0f);
			}

			if( m_blockType != GRASS )
			{
				vertexNew.position = blockLocalPositionInChunk + vec3(0.f,0.f,1.f);
				vertexNew.texCoords = vec2(topBlockMins.x, topBlockMax.y);
				vertexes.push_back(vertexNew);

				vertexNew.position = blockLocalPositionInChunk + vec3(1.f,0.f,1.f);
				vertexNew.texCoords = vec2(topBlockMax.x, topBlockMax.y);
				vertexes.push_back(vertexNew);

				vertexNew.position = blockLocalPositionInChunk + vec3(1.f,1.f,1.f);
				vertexNew.texCoords = vec2(topBlockMax.x, topBlockMins.y);
				vertexes.push_back(vertexNew);

				vertexNew.position = blockLocalPositionInChunk + vec3(0.f,1.f,1.f);
				vertexNew.texCoords = vec2(topBlockMins.x, topBlockMins.y);
				vertexes.push_back(vertexNew);
			}
		}
}


//Access Functions
vec2 Block::GetTexCoordMinsForTextureAtlasPosition( IntVec2 TexPosition )
{
	int Number_Of_Columns = 32;
	int Number_Of_Rows = 32;

	float tileHeight = 1.f / static_cast<float>(Number_Of_Rows);
	float tileWidth = 1.f / static_cast<float>(Number_Of_Columns);

	vec2 minimum = vec2( tileWidth * static_cast<float>(TexPosition.x), tileHeight * static_cast<float>(TexPosition.y) );

	return minimum;
}


vec2 Block::GetSideMinCoordsForTextureBlock(unsigned char blockType)
{
	if( blockType == DIRT )
		return GetTexCoordMinsForTextureAtlasPosition(IntVec2(19, 19));

	if( blockType == GRASS )
		return GetTexCoordMinsForTextureAtlasPosition(IntVec2(7,2));

	if( blockType == STONE )
		return GetTexCoordMinsForTextureAtlasPosition(IntVec2(17, 19));

	return GetTexCoordMinsForTextureAtlasPosition(IntVec2(0,0));
}


vec2 Block::GetTopMinCoordsForTextureBlock(unsigned char blockType)
{
	if( blockType == DIRT || blockType == GRASS )
		return GetTexCoordMinsForTextureAtlasPosition(IntVec2(23, 21));

	if( blockType == STONE )
		return GetTexCoordMinsForTextureAtlasPosition(IntVec2(17, 19));

	return GetTexCoordMinsForTextureAtlasPosition(IntVec2(0,0));
}


vec2 Block::GetBottomMinCoordsForTextureBlock(unsigned char blockType)
{
	if( blockType == DIRT || blockType == GRASS )
		return GetTexCoordMinsForTextureAtlasPosition(IntVec2(18, 19));

	if( blockType == STONE )
		return GetTexCoordMinsForTextureAtlasPosition(IntVec2(17, 19));

	return GetTexCoordMinsForTextureAtlasPosition(IntVec2(0,0));
}


void Block::BreakABlock()
{
	m_blockType = AIR;
}


void Block::PlaceABlock()
{
	m_blockType = STONE;
}