#ifndef INCLUDE_MAPCHUNK
#define INCLUDE_MAPCHUNK

#include "IntVector2.hpp"
#include "IntVec3.hpp"
#include "Block.hpp"
#include "Vertex.hpp"
#include <vector>

typedef IntVec2 ChunkCoords;

const int BLOCKS_WIDE = 16;
const int BLOCKS_TALL = 128;
const int BLOCKS_PER_CHUNK = BLOCKS_WIDE * BLOCKS_WIDE * BLOCKS_TALL;
const int BLOCK_LEVEL_TOTALS = BLOCKS_WIDE * BLOCKS_WIDE;


class MapChunk
{
public:
	MapChunk(IntVec2 chunkCoordinates);
	MapChunk() {};
	~MapChunk() {};

	//Initialize, Update, and Render
	void Render();
	void ApplyTextureVertexesToBlocks();
	void Update();
	void InitializeID();

	void GenerateChunkFromNoise();
	bool LoadChunkFromFile(const ChunkCoords& chunkCoordinates);

	const IntVec3 GetLocalCoordsFromBlockIndex( const int localIndex);
	vec2 GetBlockWorldCoordsFromIndex(int blockIndex);
	int GetBlockIndexFromBlockLocalCoords(const IntVec3& blockLocalCoords);
	int GetBlockIndexFromBlockLocalCoordsWithVec3(const vec3& blockLocalCoords);
	IntVec3 GetLocalCoordsFromWorldCoords(const vec3& worldCoords);
	ChunkCoords GetChunkCoordsFromWorldCoords( const vec2& worldPosition);
	vec3 GetBlockWorldCoordsFromIndexToVec3(int blockIndex);
	IntVec2 GetChunkWorldCoordsFromLocalCoords(ChunkCoords theChunk);
	IntVec3 GetLocalCoordinatesForBlockWorldCoords(const IntVec3& blockWorldCoords);


	//Getting Block Neighbors
	Block* GetNeighborBlockAbove(int blockIndex);
	Block* GetNeighborBlockToTheRight(int blockIndex);
	Block* GetBlockNeighborBelow(int blockIndex);
	Block* GetBlockNeighborToLeft(int blockIndex);
	Block* GetBlockNeighborInFront(int blockIndex);
	Block* GetBlockNeighborBehind(int blockIndex);



	//Member Variables
	int m_amountOfVertexes;
	bool m_isChunkVBODirty;
	unsigned int m_vboID;
	ChunkCoords m_chunkCoords;
	Block m_blocks[BLOCKS_PER_CHUNK];
	float columnHeights[BLOCK_LEVEL_TOTALS];
};
#endif